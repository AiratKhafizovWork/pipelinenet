import 'package:pipelinenet/src/blackbox.dart';
import 'package:pipelinenet/src/idto_streampoint.dart';
import 'package:pipelinenet/src/point.dart';
import 'package:test/test.dart';
import 'package:pipelinenet/src/blackbox_cfg_extension.dart';

class DTOSP_String implements IDTOStreamPoint {
  String _targetStream;
  String? get targetStream => _targetStream;
  String data;
  DTOSP_String(this._targetStream, this.data);
}

class DTOSP_Int implements IDTOStreamPoint {
  String _targetStream;
  String? get targetStream => _targetStream;
  int data;
  DTOSP_Int(this._targetStream, this.data);
}

class TestPoint extends BlackBox {
  TestPoint() {
    addInputPoint(
        'in',
        StreamPoint<DTOSP_String>(eStreamPointType.INPUT,
            handler: (DTOSP_String input) {
          print(
              'TestPoint in get ${input.data} and send lenght away ${input.data.length}');
          return input.data.length;
        }));
    addOutputPoint('out', StreamPoint<int>(eStreamPointType.OUTPUT));
    addInputPoint(
        'int_to_String',
        StreamPoint<DTOSP_Int>(eStreamPointType.INPUT,
            handler: (DTOSP_Int input) {
          print(
              'TestPoint int_to_String  get ${input.data} and send string away ${input.data.toString()}');
          return input.data.toString();
        }));
    addOutputPoint(
        'int_to_String_out', StreamPoint<String>(eStreamPointType.OUTPUT));

    this['in']?.connect(this['out']!.stream); //TODO do beatuful plz
    this['int_to_String']!
        .connect(this['int_to_String_out']!.stream); //TODO do beatuful plz

    this +
        StreamPoint<DTOSP_String>(eStreamPointType.INPUT, name: 'in+',
            handler: (DTOSP_String input) {
          print(
              'TestPoint in+ get ${input.data} and send lenght away ${input.data.length}');
          return input.data.length;
        });
    this + StreamPoint<int>(eStreamPointType.OUTPUT, name: 'out+');
  }
}

Future<void> main() async {
  TestPoint t = TestPoint();
  var o1 = t.getOutputStreamPoint<int>('out');
  var o2 = t.getOutputStreamPoint<String>('int_to_String_out');
  t.addOutputPoint(
      'int_to_String_out', StreamPoint<String>(eStreamPointType.OUTPUT));

  if (o1 != null)
    o1.stream.listen((value) {
      print('Answer from out = $value');
    });
  if (o2 != null)
    o2.stream.listen((value) {
      print('Answer from int_to_String_out = $value');
    });
  t << DTOSP_String('in', 'test');
  t << DTOSP_Int('int_to_String', 10);
  t << DTOSP_String('in+', 'test');
  await Future.delayed(Duration(seconds: 1));
}
