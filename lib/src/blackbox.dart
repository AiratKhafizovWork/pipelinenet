import 'package:meta/meta.dart';
import 'package:pipelinenet/pipelinenet.dart';
import 'package:pipelinenet/src/idto_streampoint.dart';

import 'package:pipelinenet/src/point.dart';

class BlackBox {
  Map<String, StreamPoint> _inputPoints = Map<String, StreamPoint>();
  @protected
  Map<String, StreamPoint> get inputPoints => _inputPoints;

  Map<String, StreamPoint> _outputPoints = Map<String, StreamPoint>();
  @protected
  Map<String, StreamPoint> get outputPoints => _outputPoints;

  StreamPoint<T>? getInputStreamPoint<T>(String name) {
    if (inputPoints.containsKey(name))
      return inputPoints[name] as StreamPoint<T>?;
    else
      return null;
  }

  StreamPoint<T>? getOutputStreamPoint<T>(String name) {
    if (outputPoints.containsKey(name))
      return outputPoints[name] as StreamPoint<T>;
    else
      return null;
  }

  ///Search in input and output StreamPoints
  StreamPoint? operator [](String name) {
    var tmp = getInputStreamPoint(name);
    if (tmp == null)
      return getOutputStreamPoint(name);
    else
      return tmp;
  }

  void operator <<(IDTOStreamPoint p) {
    if (p.targetStream != null) {
      var tmp = getInputStreamPoint(p.targetStream!);
      tmp?.stream.add(p);
    }
  }
}
