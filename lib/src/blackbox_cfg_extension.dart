import 'package:meta/meta.dart';
import 'package:pipelinenet/pipelinenet.dart';

extension ExtensionBlackBox on BlackBox {
  @protected
  void setInputPoint(String name, StreamPoint sp) {
    inputPoints[name] = sp;
  }

  @protected
  void setOutputPoint(String name, StreamPoint sp) {
    outputPoints[name] = sp;
  }

  @protected
  bool addInputPoint(String name, StreamPoint sp) {
    if (inputPoints.containsKey(name)) return false;
    inputPoints[name] = sp;

    return true;
  }

  @protected
  bool addOutputPoint(String name, StreamPoint sp) {
    if (outputPoints.containsKey(name)) return false;
    outputPoints[name] = sp;
    return true;
  }

  @protected
  bool removeInputPoint(String name) {
    if (!inputPoints.containsKey(name)) return false;
    inputPoints.remove(name);
    return true;
  }

  @protected
  bool removeOutputPoint(String name) {
    if (!outputPoints.containsKey(name)) return false;
    outputPoints.remove(name);
    return true;
  }

  @protected
  bool connectInputPointToOutputPoint(String in_name, String out_name) {
    if (outputPoints.containsKey(out_name) &&
        inputPoints.containsKey(in_name)) {
      inputPoints[in_name]!.connect(outputPoints[out_name]!.stream);
      return true;
    }
    return false;
  }

  @protected
  void operator +(StreamPoint t) {
    if (t.name != null) {
      if (t.type == eStreamPointType.INPUT ||
          t.type == eStreamPointType.DEBUG_INPUT)
        addInputPoint(t.name!, t);
      else if (t.type == eStreamPointType.OUTPUT ||
          t.type == eStreamPointType.DEBUG_OUTPUT) addOutputPoint(t.name!, t);
    }
  }

  @protected
  void operator -(StreamPoint t) {
    if (t.name != null) {
      if (t.type == eStreamPointType.INPUT ||
          t.type == eStreamPointType.DEBUG_INPUT)
        removeInputPoint(t.name!);
      else if (t.type == eStreamPointType.OUTPUT ||
          t.type == eStreamPointType.DEBUG_OUTPUT) removeOutputPoint(t.name!);
    }
  }
}
