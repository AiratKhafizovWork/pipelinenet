import 'package:pipelinenet/src/pipeline.dart';
abstract class IManifold
{
  // IPipeLine? getPipeLine(String name);
  bool contains(String name);
  IPipeLine? operator [](String name);
  void operator []=(String name,IPipeLine pipe);
  bool send(String name,dynamic data);
  bool add(String name,IPipeLine pipe);
  void remove(String name);
  // String? get name;
}

///"Гребенка" or Коллектор всяких PipeLine
///кста ламинарные потоки не смешиваются вроде как поэтому коллектор и использую в названии 
class Manifold implements IManifold
{
  Map<String,IPipeLine> _pipelines = Map<String,IPipeLine>();
 Map<String,IPipeLine> get pipelines =>_pipelines;
  bool contains(String name)
  {
    return _pipelines.containsKey(name);
    
  }
  IPipeLine? operator [](String name)
  {
    if(_pipelines.containsKey(name))
    return _pipelines[name];
    else
    return null;
  }
  void operator []=(String name,IPipeLine pipe)
  {
    // print('Add pipeline $name ${pipe.hashCode}');
    _pipelines[name] = pipe;
  }
  bool send(String name,dynamic data)
  {
    var pipe = this[name];
    // print('Send to ${pipe.hashCode} $data');
    if(pipe !=null)
    {

      try {
       pipe.input.add(data);
        return true; 
      } catch (e) {
        print('Error Send to $name pipeline !!! Input type != send_data type');
        return false;
      }
    }
    return false;
  }
  bool add(String name,IPipeLine pipe)
  {
    if(_pipelines.containsKey(name))
    return false;
    else
    {
      _pipelines[name] = pipe;
      return true;
    }
  }
  void remove(String name)
  {
    if(_pipelines.containsKey(name))
    {
      _pipelines.remove(name); 
    }
  }
}