import 'dart:async';

import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';

enum eStreamPointType {
  INPUT,
  OUTPUT,
  DEBUG_INPUT,
  DEBUG_OUTPUT,
}

///Точка обрабатывает входное событие полученное в поток с пмощью
///handler если обработчик возвращает значение и присутствует link то он передает
///в link выход обработчика.
///@attention Обработчик и линки имеют только входные точки!
class StreamPoint<T> {
  bool _isRun = false;
  bool get isRun => _isRun;

  eStreamPointType _type;
  eStreamPointType get type => _type;

  Map<int, PublishSubject>? _subcription;
  @protected
  String? name;
  // String? get name => _name;
  final PublishSubject<T> stream = PublishSubject<T>();
  PublishSubject? _link;

  ///связь с выходной точкой
  PublishSubject? get link =>
      _type == eStreamPointType.INPUT || _type == eStreamPointType.DEBUG_INPUT
          ? _link
          : null;

  dynamic Function(T obj)? _handler;

  dynamic Function(T obj)? get handler =>
      _type == eStreamPointType.INPUT || _type == eStreamPointType.DEBUG_INPUT
          ? _handler
          : null;

  StreamPoint(this._type,
      {dynamic Function(T obj)? handler, String? name, bool run = true})
      : _handler = handler,
        _isRun = run,
        name = name {
    if (_type == eStreamPointType.OUTPUT ||
        _type == eStreamPointType.DEBUG_OUTPUT) {
      _subcription = Map<int, PublishSubject>();
    }
    stream.listen((value) {
      if (handler != null && _isRun) {
        try {
          var ret = handler(value);
          if (ret != null && link != null) {
            link!.add(ret);
          }
        } catch (e) {
          print('StreamPoint exception:$e');
        }
      } else if (_subcription != null && _isRun) {
        var tmp = Map<int, PublishSubject>.from(_subcription!);
        tmp.forEach((key, sub) {
          sub.add(value);
        });
      }
    });
  }
  void start() {
    _isRun = true;
  }

  void stop() {
    _isRun = false;
  }

  ///link connect input point to inner output point
  void setLink(PublishSubject stream) {
    _link = stream;
  }

  ///connect output point to extern stream
  bool connect(PublishSubject stream) {
    if ((_type == eStreamPointType.OUTPUT ||
            _type == eStreamPointType.DEBUG_OUTPUT) &&
        _subcription != null) {
      if (_subcription!.containsKey(stream.hashCode)) return true;
      _subcription![stream.hashCode] = stream;
    }
    return false;
  }

  void disconnect(PublishSubject stream) {
    if (_subcription!.containsKey(stream.hashCode)) {
      _subcription!.remove(stream.hashCode);
    }
  }

  ///add to stream
  void operator(T obj) {
    stream.add(obj);
  }
}
