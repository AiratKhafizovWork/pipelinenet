import 'dart:async';

import 'package:rxdart/subjects.dart';

abstract class IPipeLine<I,O>
{
  ///Если обработчик подключен и дано разрешение на работу
  bool get isRun;
  ///Входной поток. Данные с него подаются на вход обработчика
  PublishSubject<I> get input;
  ///Выходной поток данных после обработчика
  PublishSubject<O> get output;
  ///Разрешения на обработку и подключения к обработчику выходного потока
  Future<void> startPipeline();
  ///Блокирование работы обработчика
  Future<void> stopPipeline();
  /// in-A-out connect in-B(pipe)-out  
  // void connect<X>(IPipeLine<O,X> pipe);
  void connect(IPipeLine<O,dynamic> pipe);
  Future<void> disconnect(IPipeLine pipe);
  
}


class _PipeLineConnection<O>
{
  late StreamSubscription<O>_listener;
  _PipeLineConnection(PublishSubject<O> source_stream,PublishSubject<O> target_stream)
  {
   print('_PipeLineConnection');
    _listener = source_stream.listen((value) {
      target_stream.add(value);
     });
  }
  Future<void> diconnect()
  {
    return _listener.cancel();
  }
 
}
class PipeLine<I, O> implements IPipeLine<I,O>
{
  bool _isRun = false;
  bool _isReadyToRun = false;
  PublishSubject<I> _input = PublishSubject<I>();
  PublishSubject<O> _output = PublishSubject<O>();
  late O? Function(I obj)? _handler;
  
  // Type get inputType => I.runtimeType;
  // Type get outputType => O.runtimeType;

  Map<int,_PipeLineConnection> _connections = Map<int,_PipeLineConnection>();
  bool get isRun => _isRun;
  PublishSubject<I> get input => _input;
  PublishSubject<O> get output => _output;
  StreamSubscription<I>? _listener;
  bool _isCancelListenerRun=false;

  set handler(O? Function(I obj) f) {
     _handler = f;
     _isReadyToRun = true;
  }

  PipeLine({O? Function(I obj)? handler,bool needStart=true})
   {
    
      if(handler != null)
     {
       _handler = handler;
      //  _listener = _input.listen(_topHandler);
       _isReadyToRun = true;
       if(needStart)
       startPipeline(); 
     }
   }

Future<void> startPipeline() async
{
  if(!_isRun)
  {
    // print('Start pipe ${this.hashCode}');
    _listener = _input.listen(_topHandler);
    _isRun = true;
  }
}
  Future<void> stopPipeline() async
  {
      if(_isRun && _isReadyToRun)
      {
        _isReadyToRun = false;
        await _listener!.cancel();
        _isRun = false;
        if(_handler !=null)
        _isReadyToRun = true;
      }
  }
  void _topHandler(I input)
  {
    //  print('_topHandler ${this.hashCode}');
      if (_isRun && _isReadyToRun) 
      {
        var ret = _handler!(input);
        if (ret != null) _output.add(ret);
      }
  }
  //TODO проверить будет ли работать connect(IPipeLine<O,dynamic> pipe)
  // void connect<X>(IPipeLine<O,X> pipe)
  // {
  //   if(!_connections.containsKey(pipe.hashCode))
  //   {
  //     _connections[pipe.hashCode] = _PipeLineConnection<O>(output,pipe.input);
  //   }

  // }
   void connect(IPipeLine<O,dynamic> pipe)
  {
    if(!_connections.containsKey(pipe.hashCode))
    {
      // print("Connect ${this.hashCode} to ${pipe.hashCode}");
      _connections[pipe.hashCode] = _PipeLineConnection<O>(output,pipe.input);
    }

  }
  Future<void> disconnect(IPipeLine pipe)async
  {
    if(_connections.containsKey(pipe.hashCode))
    {
      await _connections[pipe.hashCode]!.diconnect();
    }
  }
  
}
